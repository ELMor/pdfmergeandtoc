package isf.pdf.tool;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class MergeAndTOC {

	public static void main(String[] args) {
		for (int k = 0; k < args.length; k++) {
			File dir = new File(args[k]);
			if (!dir.exists()) {
				System.out.println("No existe el directorio " + args[k]);
				return;
			}
			if (!dir.isDirectory()) {
				System.out.println(args[k] + " no es un directorio!");
				return;
			}
			try {
				String []l=dir.list();
				for(int i=0;i<l.length;i++){
					File d2=new File(dir,l[i]);
					if(d2.isDirectory())
						wBuild(d2.getAbsolutePath(),dir.getAbsolutePath()+"-"+l[i]+".pdf");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void wBuild(final String dir, String pdfName) throws Exception {
		Entrada padre=new Entrada(null,dir,new String[]{".pdf"});
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(new File(pdfName)));
		writer.setFullCompression();
		document.addTitle(getLastName(dir));
		document.addAuthor(getLastName(dir));
		document.open();
		createToC(document,padre);
		concatenaPDFs(document,writer,padre);
		document.close();
		System.out.println("Fin");
	}

	private static void createToC(Document doc, Entrada ent) throws DocumentException{
		System.out.println("Creando ToC para "+ent.getCanonicalName());
		doc.newPage();
		String Titulo="0./"+ent.getCanonicalName();
		addPara(doc,Titulo,Paragraph.ALIGN_LEFT,
				(ent.getPadre()!=null?ent.getPadre().getCanonicalName():null),ent.getCanonicalName(),true);
		ArrayList al=ent.getHijos();
		for(int i=0;i<al.size();i++){
			Entrada hij=(Entrada)al.get(i);
			String kk=""+(i+1)+".";
			if(hij.isDir())
				kk+="(+)";
			kk+=hij.getPres();
			addPara(doc,kk,Paragraph.ALIGN_LEFT,hij.getCanonicalName(),null,false);
		}
		for(int i=0;i<al.size();i++){
			Entrada hij=(Entrada)al.get(i);
			if(hij.isDir())
				createToC(doc,hij);
		}
	}

	private static void addPara(
				Document doc, 
				String txt, 
				int ali,
				String localGoto,
				String localDest,
				boolean subr) throws DocumentException {
		Chunk chunk;
		Paragraph para;
		chunk = new Chunk( txt );
		chunk.setFont(new Font(1));
		if(subr)
			chunk.setUnderline(Color.BLACK,0.00f,0.075f,0,-0.2f,PdfContentByte.LINE_CAP_ROUND);
		if(localGoto!=null)
			chunk.setLocalGoto(localGoto);
		if(localDest!=null)
			chunk.setLocalDestination(localDest);
		para = new Paragraph(chunk);
		para.setAlignment(ali);
		para.setSpacingAfter(20);
		doc.add(para);
	}
	
	private static void concatenaPDFs(Document doc, PdfWriter writer, Entrada ent) throws Exception{
		ArrayList al=ent.getHijos();
		PdfContentByte cb = writer.getDirectContent();
		for(int i=0;i<al.size();i++){
			Entrada hij=(Entrada)al.get(i);
			if(hij.isSelectedResource()==-1)
				continue;
			PdfReader reader = new PdfReader(new FileInputStream(hij.getFile()));
			int numPagesToImport = reader.getNumberOfPages();
			for (int j = 1; j <= numPagesToImport; j++) {
				doc.newPage();
				PdfImportedPage page = writer.getImportedPage(reader, j);
				if (j == 1) {
					cb.localDestination(hij.getCanonicalName(),
							new PdfDestination(PdfDestination.FIT));
					System.out.println("\tAņadiendo " + hij.getCanonicalName());
				}
				cb.addTemplate(page, 0, 0);
			}
			reader.close();
		}
		for(int i=0;i<al.size();i++){
			Entrada hij=(Entrada)al.get(i);
			if(hij.isDir())
				concatenaPDFs(doc,writer,hij);
		}
	}
	
	private static String getLastName(String dir){
		int x=dir.lastIndexOf("\\");
		if(x>0 && x+1<dir.length() )
			dir=dir.substring(x+1);
		x=dir.lastIndexOf("/");
		if(x>0 && x+1<dir.length() )
			dir=dir.substring(x+1);
		return dir;
	}
	
}
